import { createApp } from 'vue'
import App from './App.vue'
import './assets/style/app.scss'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faUserSecret } from '@fortawesome/free-solid-svg-icons'
import { fas } from "@fortawesome/free-solid-svg-icons";

library.add(faUserSecret);
library.add(fas);

createApp(App)
.component('font-awesome-icon', FontAwesomeIcon)
.mount('#app')

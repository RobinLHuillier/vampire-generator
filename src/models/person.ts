export default interface Person {
    name: string,
    surname: string,
    age: number,
    sex: string,
    resonance: string,
    dyscrasie: string,
    photoPath: string,
    activity: {
        name: string,
        description: string,
    },
    will: string,
}
export default function getWill(): string[] {
    return [
        'Jeunesse éternelle',
        'Nouveau boulot',
        'Une grève',
        'L\'amour',
        'La révolution',
        'De l\'alcool',
        'Ne pas être vu là maintenant',
        'Fuguer',
        'Devenir poête',
        'La mort',
        'De l\'ordre dans sa vie',
        'Comprendre comment tout a pu merder',
        'Baiser',
        'Quelque chose de permanent',
        'Joindre les deux bouts',
        'L\'objet qu\'il a vendu pour payer le loyer',
        'Quelqu\'un à l\'écoute',
        'L\'approbation de ses amis',
        'Rentrer à la maison',
    ];
}
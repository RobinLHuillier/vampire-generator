import Person from "@/models/person";
import getResonance, { getDyscrasie } from "./resonance";
import getPrenomsFem20 from "./prenoms-fem-20";
import getPrenomsFem50 from "./prenoms-fem-50";
import getPrenomsMasc20 from "./prenoms-masc-20";
import getPrenomsMasc50 from "./prenoms-masc-50";
import getNom from "./noms";
import { getActivity } from "./activite";
import getWill from "./volonte";

const maxMale20 = 17;
const maxFemale20 = 12;
const maxMale50 = 10;
const maxFemale50 = 9;

function rand(max: number, min?: number) {
    if (undefined === min) {
        min = 0;
    }
    return Math.floor(Math.random() * (max-min)) + min;
}

function randElem<Type>(array: Type[]): Type {
    return array[rand(array.length)];
}

function delElem<Type>(array: Type[], elem: Type) {
    if (array.indexOf(elem) === -1) {
        console.error("[delElem] trying to delete an element non existent", array, elem);
        return;
    }
    array.splice(array.indexOf(elem), 1);
}

export default function getRandPerson(
    sexRequired?: 'male' | 'female' | 'both',
    ageRange?: 'young' | 'old' | 'both',
): Person {
    let sex: string;
    if (undefined === sexRequired || 'both' === sexRequired) {
        sex = randElem(["male", "female"]);
    } else {
        sex = sexRequired;
    }
    const name = randElem(getNom());
    let age: number;
    if (undefined === ageRange || 'both' === ageRange) {
        age = rand(70, 16);
    } else {
        age = 'young' === ageRange ? rand(40, 16) : rand(70, 40);
    }
    let surname: string;
    let photoPath = "";
    if (sex === "male") {
        if (age < 40) {
            surname = randElem(getPrenomsMasc20());
            photoPath += "male20/" + rand(maxMale20+1, 1).toString();
        } else {
            surname = randElem(getPrenomsMasc50());
            photoPath += "male50/" + rand(maxMale50+1, 1).toString();
        }
    } else {
        if (age < 40) {
            surname = randElem(getPrenomsFem20());
            photoPath += "female20/" + rand(maxFemale20+1, 1).toString();
        } else {
            surname = randElem(getPrenomsFem50());
            photoPath += "female50/" + rand(maxFemale50+1, 1).toString();
        }
    }
    photoPath += ".png";
    const resonance = randElem(getResonance());
    const dyscrasie = randElem(getDyscrasie());
    const activity = randElem(getActivity());
    const will = randElem(getWill());
    return {
        name: name,
        surname: surname,
        age: age,
        sex: sex,
        resonance: resonance,
        dyscrasie: dyscrasie,
        activity: activity,
        will: will,
        photoPath: photoPath,
    }
}
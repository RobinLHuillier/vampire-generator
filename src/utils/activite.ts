export function getActivity(): {
    name: string,
    description: string,
}[] {
    return [
        {
            name: 'Acteur',
            description: 'Physique agréable, a participé à une pub',
        }, {
            name: 'Agent de sécurité',
            description: 'Uniforme, badge, lampe-torche, routine',
        }, {
            name: 'Agent de propreté',
            description: 'Conduit le camion-benne',
        }, {
            name: 'Amant',
            description: 'Entrelacé avec son amant',
        }, {
            name: 'Anarchiste',
            description: 'Bandana noir, spray dans le sac, chaussure de chantier à bout métallique',
        }, {
            name: 'Artiste',
            description: 'En route vers un vernissage organisé par quelqu\'un de moins talentueux',
        }, {
            name: 'Acteur',
            description: 'Physique agréable, a participé à une pub',
        }, {
            name: 'Barman',
            description: 'Chemise négligé ouverte, juste en pause',
        }, {
            name: 'Bibliothécaire',
            description: 'Rentre d\'un endroit encore plus barbant que chez soi',
        }, {
            name: 'Cambrioleur',
            description: 'En route vers la prochaine effraction',
        }, {
            name: 'Capitaliste en herbe',
            description: 'En fugue',
        }, {
            name: 'Chauffeur de bus',
            description: 'Juste un volant en uniforme',
        }, {
            name: 'Chef d\'antenne de parti',
            description: 'Dégotte un poste au sein de la municipalité',
        }, {
            name: 'Chômeur à vie',
            description: 'Son métier est devenu obsolète. Au moins la bière bon marché reste bon marché',
        }, {
            name: 'Clubeur',
            description: 'Sort le soir, va danser, draguer, conclure, et recommencer.',
        }, {
            name: 'Cocu',
            description: 'Sait que l\'autre le trompe, va régler ça en hurlant',
        }, {
            name: 'Coursier à vélo',
            description: 'Toujours pressé, prend le pourboire, passe à la suivante.',
        },
    ];
}
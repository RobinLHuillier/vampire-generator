export default function getResonance(): string[] {
    return [
        'Atrabilaire',
        'Bilieuse',
        'Phlegmatique',
        'Sanguine',
    ];
}

export function getDyscrasie(): string[] {
    return [
        'fugace',
        'intense',
        'vif',
    ];
}